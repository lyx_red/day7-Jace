package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.EmployeeUpdateException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.stubbing.OngoingStubbing;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_created_employee_when_create_employee_given_employee_age_is_between_18_to_65(Integer age) {
        //given
        Employee employee = new Employee(null, "Lily", age, "Female", 20000);
        //when
        when(employeeRepository.addEmployee(employee)).thenReturn(new Employee(1L, "Lily", age, "Female", 20000));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
        //then
    }

    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_to_65(int age) {
        //given
        Employee employee = new Employee(null, "Lily", age, "Female", 20000);
        //when
        EmployeeCreatedException employeeCreatedException = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));

        assertEquals("Employee must be 18-65 years old", employeeCreatedException.getMessage());
        //then
    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30, 20000", "29,19999"})
    void should_return_created_employee_when_create_employee_given_(Integer age, Integer salary) {
        //given
        Employee employee = new Employee(null, "Lily", age, "Female", salary);
        //when
        when(employeeRepository.addEmployee(employee)).thenReturn(new Employee(1L, "Lily", age, "Female", salary));
        //then
        Employee employeeResponse = employeeService.create(employee);
        assertNotNull(employeeResponse);
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(salary, employeeResponse.getSalary());
    }

    @Test
    void should_throw_exception_when_create_employee_given_employee_age_i_30_and_salary_is_19999() {
        //given
        Employee employee = new Employee(null, "Lily", 30, "Female", 19999);
        //when
        EmployeeCreatedException employeeCreatedException = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));

        assertEquals("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created", employeeCreatedException.getMessage());
    }

    @Test
    void should_create_employee_and_active_status_is_true_when_create_employee_given_employee() {
        //given
        Employee employee = new Employee(null, "Jace", 21, "Male", 8000);

        //when
        when(employeeRepository.addEmployee(employee)).thenReturn(new Employee(1L, "Jace", 21, "Male", 8000, true));
        //then
        Employee employeeResponse = employeeService.create(employee);
        assertNotNull(employeeResponse);
        assertEquals("Jace", employeeResponse.getName());
        assertEquals(21, employeeResponse.getAge());
        assertEquals("Male", employeeResponse.getGender());
        assertEquals(8000, employeeResponse.getSalary());
        assertTrue(employeeResponse.isActiveStatus());
    }

    @Test
    void should_delete_employee_and_active_status_is_false_when_delete_employee_given_employee() {
        //given
        Long employeeId = 1L;
        when(employeeRepository.getEmployeeById(employeeId)).thenReturn(new Employee(1L, "Jace", 21, "Male", 8000, true));

        Employee employeeResponse = employeeService.delete(employeeId);
        assertNotNull(employeeResponse);
        assertEquals("Jace", employeeResponse.getName());
        assertEquals(21, employeeResponse.getAge());
        assertEquals("Male", employeeResponse.getGender());
        assertEquals(8000, employeeResponse.getSalary());
        assertFalse(employeeResponse.isActiveStatus());
    }

    // When updating an employee, you need to verify whether the employee is active or not,
    // if he/she has already left the company, you can't update him/her.

    @Test
    void should_update_employee_when_update_employee_given_employee_and_active_is_true() {
        //given
        Employee employee = new Employee(1L, "Jace", 21, "Male", 8000, 1L, true);
        Employee newEmployee = new Employee(1L, "Jace", 22, "Male", 10000, 2L, true);
        when(employeeRepository.getEmployeeById(employee.getId())).thenReturn(employee);
        when(employeeRepository.updateEmployee(employee.getId(), newEmployee)).thenReturn(newEmployee);

        Employee updateEmployee = employeeService.update(employee.getId(), newEmployee);
        assertNotNull(updateEmployee);
        assertEquals("Jace", updateEmployee.getName());
        assertEquals(22, updateEmployee.getAge());
        assertEquals(10000, updateEmployee.getSalary());
        assertEquals(2L, updateEmployee.getCompanyId());
    }

    @Test
    void should_throw_exception_and_error_message_when_update_employee_given_employee_and_active_is_false() {
        Employee employee = new Employee(1L, "Jace", 21, "Male", 8000, 1L, false);
        Employee newEmployee = new Employee(1L, "Jace", 22, "Male", 10000, 2L, true);
        when(employeeRepository.getEmployeeById(employee.getId())).thenReturn(employee);
        when(employeeRepository.updateEmployee(employee.getId(), newEmployee)).thenReturn(newEmployee);

        assertThrows(EmployeeUpdateException.class, () -> employeeService.update(employee.getId(), newEmployee));
    }

}
