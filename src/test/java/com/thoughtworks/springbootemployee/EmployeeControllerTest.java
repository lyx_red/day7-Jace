package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
    }

    @Test
    void should_get_all_employees_when_perform_get_all_employees_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "jace", 21, "male", 1111);
        //when
        //then
        employeeRepository.addEmployee(employee);
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("jace"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(21))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(1111));

    }

    @Test
    void should_get_employee_when_perform_get_employee_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "jace", 21, "male", 1111);
        //when
        //then
        employeeRepository.addEmployee(employee);
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("jace"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(21))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(1111));

    }

    @Test
    void should_create_employee_when_perform_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "jace", 21, "male", 1111);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
        //when
        //then
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("jace"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(21))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(1111));

        Employee saveEmployee = employeeRepository.getEmployees().get(0);
        assertEquals("jace", saveEmployee.getName());
        assertEquals(21, saveEmployee.getAge());
        assertEquals("male", saveEmployee.getGender());
        assertEquals(1111, saveEmployee.getSalary());
    }

    @Test
    void should_get_employees_by_gender_when_perform_get_employees_by_gender_given_employees() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "jace", 21, "male", 1111);
        Employee employee2 = new Employee(2L, "lisa", 22, "female", 2333);
        //when
        //then
        employeeRepository.addEmployee(employee1);
        employeeRepository.addEmployee(employee2);
        client.perform(MockMvcRequestBuilders.get("/employees")
                        .param("gender", "female"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].gender").value("female"));

    }

    @Test
    void should_update_employee_when_perform_update_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "jace", 21, "male", 1111, true);
        Employee newEmployee = new Employee(1L, "jace", 23, "male", 8000, 1L);
        String newEmployeeJson = new ObjectMapper().writeValueAsString(newEmployee);
        //when
        //then
        employeeRepository.addEmployee(employee);
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", employee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("jace"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(23))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(8000));
        Employee saveEmployee = employeeRepository.getEmployees().get(0);
        assertEquals("jace", saveEmployee.getName());
        assertEquals(23, saveEmployee.getAge());
        assertEquals("male", saveEmployee.getGender());
        assertEquals(8000, saveEmployee.getSalary());
        assertEquals(1L, saveEmployee.getCompanyId());
    }

    @Test
    void should_delete_employee_when_perform_delete_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "jace", 21, "male", 1111);
        //when
        //then
        employeeRepository.addEmployee(employee);
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        assertEquals(false, employeeRepository.getEmployeeById(1L).isActiveStatus());

    }

    @Test
    void should_get_all_employees_by_page_when_perform_get_all_employees_by_page_given_employees() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "jace", 21, "male", 1111);
        Employee employee2 = new Employee(2L, "lisa", 22, "female", 5000);
        Employee employee3 = new Employee(3L, "jace2", 21, "male", 8888);
        Employee employee4 = new Employee(4L, "jace3", 21, "male", 1111);
        employeeRepository.addEmployee(employee1);
        employeeRepository.addEmployee(employee2);
        employeeRepository.addEmployee(employee3);
        employeeRepository.addEmployee(employee4);
        //when
        //then

        client.perform(MockMvcRequestBuilders.get("/employees").param("page", "1").param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("jace"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(21))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(1111));

    }
}
