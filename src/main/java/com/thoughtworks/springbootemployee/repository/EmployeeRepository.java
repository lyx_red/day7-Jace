package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee addEmployee(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst().orElseGet(null);
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public void deleteEmployeeById(Long id) {
        try {
            Employee employee = employees.stream()
                    .filter(employeeTemp -> employeeTemp.getId().equals(id))
                    .findFirst()
                    .orElseGet(null);
            employee.setActiveStatus(false);
        } catch (NullPointerException e) {
            System.out.println("employee is not exist");
        }
    }

    public List<Employee> getEmployeesByPage(Integer page, Integer size) {
        return employees.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(Long id, Employee newEmployee) {
        Employee employee = getEmployeeById(id);
        if (!Objects.isNull(employee)) {
            if(Objects.nonNull(newEmployee.getAge())) {
                employee.setAge(newEmployee.getAge());
            }
            if(Objects.nonNull(newEmployee.getSalary())) {
                employee.setSalary(newEmployee.getSalary());
            }
            if(Objects.nonNull(newEmployee.getCompanyId())) {
                employee.setCompanyId(newEmployee.getCompanyId());
            }
        }
        return employee;
    }

    public void deleteEmployeesByCompanyId(Long id) {
        List<Employee> waitDeleteEmployees = employees.stream().filter(employee -> employee.getCompanyId().equals(id))
                .collect(Collectors.toList());
        waitDeleteEmployees.forEach(employees::remove);
    }

    private Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public void clearAll() {
        employees.clear();
    }
}
