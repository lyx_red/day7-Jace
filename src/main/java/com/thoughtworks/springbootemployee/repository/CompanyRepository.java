package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    @Autowired
    private EmployeeRepository employeeRepository;
    private final List<Company> companies = new ArrayList<>();

    public List<Company> getCompanies() {
        return companies;
    }

    public Company addCompany(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public Company getCompanyById(Long id) {
        return companies.stream().filter(company -> company.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Company> getCompaniesByPage(Integer page, Integer size) {
        return companies.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public void deleteCompanyById(Long id) {
        try {
            Company company = companies.stream()
                    .filter(companyTemp -> companyTemp.getId().equals(id))
                    .findFirst()
                    .orElseGet(null);
            employeeRepository.deleteEmployeesByCompanyId(id);
            companies.remove(company);
        } catch (NullPointerException e) {
            System.out.println("company is not exist");
        }
    }

    public void clearAll() {
        companies.clear();
    }
}
