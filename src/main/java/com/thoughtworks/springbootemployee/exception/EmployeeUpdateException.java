package com.thoughtworks.springbootemployee.exception;

public class EmployeeUpdateException extends RuntimeException {
    public EmployeeUpdateException(String message) {
        super(message);
    }
}
