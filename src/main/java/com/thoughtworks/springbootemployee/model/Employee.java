package com.thoughtworks.springbootemployee.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Long id;

    private String name;

    private Integer age;

    private String gender;

    private Integer salary;

    private Long companyId;

    private boolean activeStatus;

    public Employee(Long id, String name, Integer age, String gender, Integer salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.companyId = companyId;
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary, boolean activeStatus) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.activeStatus = activeStatus;
    }

    public boolean isAgeRangeInvalid() {
        return getAge() < 18 || getAge() > 65;
    }

    public boolean isAgeAndSalaryRangeInvalid() {
        return  getAge() >= 30 && getSalary() < 20000;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) && Objects.equals(name, employee.name) && Objects.equals(age, employee.age) && Objects.equals(gender, employee.gender) && Objects.equals(salary, employee.salary) && Objects.equals(companyId, employee.companyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, gender, salary, companyId);
    }
}
