package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.EmployeeUpdateException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmployeeService {
    public static final String EMPLOYEES_AGE_NOT_RANGE_ERROR_MESSAGE = "Employee must be 18-65 years old";
    public static final String EMPLOYEES_AGE_AND_SALARY_ERROR_MESSAGE = "Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created";
    //    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) {
        if (employee.isAgeRangeInvalid()) {
            throw new EmployeeCreatedException(EMPLOYEES_AGE_NOT_RANGE_ERROR_MESSAGE);
        }
        if (employee.isAgeAndSalaryRangeInvalid()) {
            throw new EmployeeCreatedException(EMPLOYEES_AGE_AND_SALARY_ERROR_MESSAGE);
        }
        employee.setActiveStatus(true);
        return employeeRepository.addEmployee(employee);
    }

    public Employee delete(Long employeeId) {
        Employee employee = employeeRepository.getEmployeeById(employeeId);
        employee.setActiveStatus(false);
        return employee;
    }

    public Employee update(Long id, Employee employee) {
        Employee existEmployee = employeeRepository.getEmployeeById(id);
        if (!existEmployee.isActiveStatus()) {
            throw new EmployeeUpdateException("this user has already left the company, you can't update.");
        }
        return employeeRepository.updateEmployee(id, employee);
    }

    public List<Employee> getEmployees() {
        return employeeRepository.getEmployees();
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employeeRepository.getEmployeesByGender(gender);
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.getEmployeeById(id);
    }

    public List<Employee> getEmployeesByPage(Integer page, Integer size) {
        return employeeRepository.getEmployeesByPage(page, size);
    }

    public List<Employee> getEmployeesByCompanyId(Long id) {
        return employeeRepository.getEmployeesByCompanyId(id);
    }
}
