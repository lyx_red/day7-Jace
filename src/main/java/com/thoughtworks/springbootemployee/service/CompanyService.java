package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    public Company addCompany(Company company) {
        return companyRepository.addCompany(company);
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(Long id) {
        return companyRepository.getCompanyById(id);
    }

    public List<Company> getCompaniesByPage(Integer page, Integer size) {
        return companyRepository.getCompaniesByPage(page, size);
    }

    public void deleteCompanyById(Long id) {
        companyRepository.deleteCompanyById(id);
    }
}
