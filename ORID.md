# ORID



## Objective

- This morning I focused on integration testing used in spring boot development. Before learning centralized testing, I had to go to Postman every time to test every interface, which was very inconvenient. After learning integration testing, I felt that it was very beneficial to my development efficiency.
- In the afternoon, I mainly learned a hierarchical concept about spring boot, which made me more clear about the development process of spring boot and what each layer should do. At the same time, I also did a lot of exercises about spring boot and integration test.



## Reflective

- I felt very confused and labored


## Interpretative

- I'm having a little trouble with the integration testing part. At the same time, when I retest the Service layer, I often get confused about whether I should use "when" or "verify", and get confused by mock tests.

## Decision

- I will continue to learn integration testing, and at the same time ask teachers and classmates for advice, hoping to have a further understanding and learning of integration testing.